import React from 'react';
import ReactDOM from 'react-dom';
import Geosuggest from 'react-geosuggest';
import * as locale from '../../locales/locale_en'
import './geoSuggest.css'

export default class GeoSuggest extends React.Component {

    constructor(props) {
        super(props);
        this.onSuggestSelect = this.onSuggestSelect.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    render() {
        let fixtures = [/*
            {label: 'Old Elbe Tunnel, Hamburg', location: {lat: 53.5459, lng: 9.966576}},
            {label: 'Reeperbahn, Hamburg', location: {lat: 53.5495629, lng: 9.9625838}},
            {label: 'Alster, Hamburg', location: {lat: 53.5610398, lng: 10.0259135}}*/
        ];

        let {initial, placeholder} = this.props;
        let location = initial.location ? new google.maps.LatLng(initial.location.lat, initial.location.lng) : null;
        let initialValue = initial.value ? initial.value : ''

        return (
            <div className={"geoSuggest_"+this.props.target}>
                <Geosuggest
                    ref={el=>this._geoSuggest=el}
                    placeholder={placeholder}
                    initialValue={initialValue}
                    fixtures={fixtures}
                    onSuggestSelect={this.onSuggestSelect}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    location={location}
                    radius="20" />
            </div>
        )
    }

    /**
     * When a suggest got selected
     * @param  {Object} suggest The suggest
     */
    onSuggestSelect(suggest) {
        this.props.onSuggestSelect(suggest);
    }

    onFocus() {
        this.props.onFocus();
    }

    onBlur() {
        this.props.onBlur();
    }
}