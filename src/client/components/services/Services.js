import React from 'react';
import ReactDOM from 'react-dom';
import './services.css'

export default class Services extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {servicesList} = this.props;
        const isFilledWithData = servicesList.length > 0;

        const listItems = servicesList.map(service => {
                const brandPrices = service.prices.map(option => {

                    return <tr className={"brandOptionPrice"} key={'price'+option.name + option.cost}>
                        <td><span className={"optionName"}>{option.name}</span></td>
                        <td><span className={"optionCost"}>{option.cost}</span></td>
                    </tr>;
                });

                return <div className={"brandWrapper"} key={'brand' + service.name}>
                    <div className={'brandLogo brandLogo_'+service.name}></div>
                    <div className={"brandPrices"}>
                        <table>
                            <tbody>{brandPrices}</tbody>
                        </table>
                    </div>
                </div>
            }
        );

        return (
            <div className={"servicesWrapper"}>
                {listItems}
            </div>
        )
    }
}