export const RETRIEVING_LOCATION = 'Retrieving current location...';
export const ENTER_TARGET = 'Enter target destination';
export const ENTER_FROM = 'Please enter current location';
export const ADDRESS_NOT_FOUND = 'Address not found';