import React, { Component } from 'react';
import {GOOGLE_MAPS_API_KEY, LYFT_ACCESS_TOKEN, UBER_SERVER_TOKEN} from './constants/apiKeys'
import * as locale  from './locales/locale_en'
import GeoSuggest from './components/geoSuggest/GeoSuggest'
import Services from './components/services/Services'
import './app.css';

const initialServices = {
    lyft: {prices: []},
    uber: {prices: []},
};

function getAddressFromLatLng(location) {
    return fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${location.lat},${location.lng}&key=${GOOGLE_MAPS_API_KEY}`)
        .then(response => response.json())
        .catch(error => {
            console.error(error);
        });
}

function getUberPriceFromLatLng(start = {lat: 37.7752315, lng: -122.418075}, end = {lat: 37.7752415, lng: -122.518075}) {
    //console.log('\nFind a drive from ', start, ' to ',  end);
    return fetch(`https://api.uber.com/v1.2/estimates/price?start_latitude=${start.lat}&start_longitude=${start.lng}&end_latitude=${end.lat}&end_longitude=${end.lng}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Token ${UBER_SERVER_TOKEN}`,
                'Accept-Language': 'en_US',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
        })
        .then(response => response.json())
        .catch(error => {
            console.error(error);
        });
}

function getLyftPriceFromLatLng(start = {lat: 59.938161, lng: 30.315535}, end = {lat: 59.9156914, lng: 30.5052749}) {
    return fetch(`https://api.lyft.com/v1/cost?start_lat=${start.lat}&start_lng=${start.lng}&end_lat=${end.lat}&end_lng=${end.lng}&ride_type=lyft`,
        {method: 'GET', headers: {'Authorization': `bearer ${LYFT_ACCESS_TOKEN}`},})
        .then(response => response.json())
        .catch(error => {
            console.error(error);
        });
}

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            language: navigator.language.split('-')[0],
            initial: {
                value: null,
                location: null,
                pending: false
            },
            start: null,
            end: null,
            services: initialServices,
            displayServices: true

        };
        this.onSuggestSelect = this.onSuggestSelect.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.cutDetailedAddress = this.cutDetailedAddress.bind(this);
        this.setServiceState = this.setServiceState.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        const prevStart = prevState.start;
        const prevEnd = prevState.end;
        const {start, end} = this.state;
        //console.log('start', start, '/n end', end);

        if(start && end) {
            if(start !== prevStart || end !== prevEnd || start.location !== prevStart.location || end.location !== prevEnd.location) {
                this.setState({
                    services: initialServices
                });

                getLyftPriceFromLatLng(start.location, end.location).then(data => {
                    if(!data || data.error) {
                        this.setServiceState('lyft', []);
                    } else if(data.cost_estimates && data.cost_estimates[0]) {
                        let estimate = data.cost_estimates[0];
                        if(estimate.error_message) {
                            this.setServiceState('lyft', []);
                        } else {
                            let cost = '$'+Math.round(estimate.estimated_cost_cents_min/100) + '-' + Math.round(estimate.estimated_cost_cents_max/100);
                            this.setServiceState('lyft', [{cost, name: estimate.display_name}]);
                        }
                    }
                });

                getUberPriceFromLatLng(start.location, end.location).then(data => {
                    if(!data || data.error || data.code === 'distance_exceeded' || (data.prices && data.prices.length === 0)) {
                        this.setServiceState('uber', false);
                    } else if(data) {
                        let prices = data.prices.map(option => {
                            return {
                                cost: option.estimate,
                                name: option.display_name
                            }
                        });
                        this.setServiceState('uber', prices);
                    }
                });
            }
        }
    }

    onSuggestSelect = (data, target) => {
        //console.log('set ', target, ' with data', data);
        if(typeof data !== 'undefined') {
            data.location.lat = Number((data.location.lat).toFixed(6));
            data.location.lng = Number((data.location.lng).toFixed(6));
            this.setState({
                [target]: {
                    ...this.state[target],
                    ...data
                },
                displayServices: true
            })
        } else {
            this.setState({
                [target]: null,
                services: initialServices
            })
        }
    };

    onFocus = (data, target) => {
        this.setState({displayServices: false})
    };

    onBlur = (data, target) => {
        this.setState({displayServices: true})
    };

    componentDidMount() {

      navigator.geolocation.getCurrentPosition(data => {
          this.setState({
              initial: {
                  ...this.state.initial,
                  pending: true
              }
          });

          getAddressFromLatLng({lat: data.coords.latitude, lng: data.coords.longitude})
          .then(data => {
              let value = this.cutDetailedAddress(data.results[0].formatted_address);
              let location = data.results[0].geometry.location;
              this.setState({
                  initial: {
                      pending: false,
                      value,
                      location
                  },
                  start: {
                      location
                  }
              });
          });

      }, error => {

      });
    }

  render() {

    const {initial, services, displayServices} = this.state;

    const servicesList = [];
    if(displayServices) {
        for (let name in services) {
            if (services.hasOwnProperty(name)) {
                if(services[name].prices.length > 0) {
                    servicesList.push({...services[name], name});
                }
            }
        }
    }

    return (
      <div className="appWrapper">
          <div className={"appCenterWrapper"}>
              <div className={"inputFieldWrapper"}>
                  <GeoSuggest
                      target={'from'}
                      initial={initial}
                      placeholder={locale.ENTER_FROM}
                      onSuggestSelect={(event) => this.onSuggestSelect(event, 'start')}
                      onFocus={(event) => this.onFocus(event, 'start')}
                      onBlur={(event) => this.onBlur(event, 'start')}
                  />
                  <GeoSuggest
                      target={'to'}
                      initial={{location: initial.location, value: null}}
                      placeholder={locale.ENTER_TARGET}
                      onSuggestSelect={(event) => this.onSuggestSelect(event, 'end')}
                      onFocus={(event) => this.onFocus(event, 'end')}
                      onBlur={(event) => this.onBlur(event, 'end')}
                  />
              </div>
              <Services
                servicesList={servicesList}
              />
          </div>
      </div>
    );
  }

    setServiceState(serviceName, prices = []) {
        this.setState({
            services: {
                ...this.state.services,
                [serviceName]: {
                    prices
                }
            }
        })
    }

    cutDetailedAddress(text, shouldLeftElements = 4) {
        return text.split(',').slice(0, shouldLeftElements).join();
    }
}
